Dependencies:  
**Node.js  
NPM**

Node dependencies:  
**fs, steam, request, libcurl**  

First launch:  
1. Put in your username and password at the bottom of script.  
2. Comment out lines:  
"authCode: userinfo.authCode, shaSentryfile: sentry" in bot.logon function.  
3. It will show error code "63" -> This means that steam guard sent you code.   
4. Stop the script and put that code in auth code place at the bottom of script and uncomment your previously commented out.  
5. If all is good, it will start to idle for cards.

**usage:** node new.js 1 (idle in alphabetical order), 2 {game_id} (idle given game), 3 (idle all games with card drops)  
node new.js 1  
node new.js 3  
node new.js 2 242880  
node new.js 2 242880 238430