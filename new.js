var fs = require('fs');
var steam = require('steam');
var request = require('request');
var sentryFile = 'sentryfile';
var sentry = undefined;

if (fs.existsSync(sentryFile)) {
    sentry = fs.readFileSync(sentryFile);
}

function updateSentry (buffer) {
    console.log(buffer);
    //fs.writeFile(sentryFile, buffer);
	stream = fs.createWriteStream(sentryFile);
	stream.write(buffer);
	stream.end();
}

function createIdler(userinfo, timer){
    var bot = new steam.SteamClient();
    userinfo.bot = bot;
    bot.on('loggedOn', function() {
        bot.setPersonaState(steam.EPersonaState.Online);
        canTrade = false;
        console.log('Logged in ' + userinfo.username);
    });
    bot.on('sentry', updateSentry);
    bot.on('error', function(e) {
        console.log(userinfo);
        console.log(e);
    });
    function startIdle(){
        var req = request.defaults({jar: userinfo.jar});
        req.get('http://steamcommunity.com/my/badges/', function (err, res, body) {
            if (body) {
			//var bb = body;
            //var bb = body.match(/<a class="btn_green_white_innerfade btn_small_thin" href="steam:\/\/run\/(\d+)">/g);
			//var b = bb[0].match(/<a class="btn_green_white_innerfade btn_small_thin" href="steam:\/\/run\/(\d+)">/);
			var b = body.match(/<a class="btn_green_white_innerfade btn_small_thin" href="steam:\/\/run\/(\d+)">/);
                if (b && b[1] != '267420') {
                    console.log(userinfo.username);
                    console.log("Idling game " + b[1]);
                    bot.gamesPlayed([b[1]]);
					var now = new Date();                
            		console.log(now.getHours()+':'+now.getMinutes()+':'+now.getSeconds());
                }				
            }
        });
    }
    bot.on('webSessionID', function (sessionID) {
        userinfo.jar = request.jar(),
        userinfo.sessionID = sessionID;
        bot.webLogOn(function(cookies) {
            cookies.forEach(function(cookie) {
                userinfo.jar.setCookie(request.cookie(cookie), 'http://steamcommunity.com');
                userinfo.jar.setCookie(request.cookie(cookie), 'http://store.steampowered.com');
                userinfo.jar.setCookie(request.cookie(cookie), 'https://store.steampowered.com');
            });
            userinfo.jar.setCookie(request.cookie("Steam_Language=english"), 'http://steamcommunity.com');

            startIdle();
            setInterval(function(){startIdle();}, timer);

        });
    });
    bot.logOn({
        accountName: userinfo.username,
        password: userinfo.password,
        authCode: userinfo.authCode,
        shaSentryfile: sentry
    });
}

function createIdler2(userinfo, timer){
    var bot = new steam.SteamClient();
    userinfo.bot = bot;
    bot.on('loggedOn', function() {
        bot.setPersonaState(steam.EPersonaState.Online);
        canTrade = false;
        console.log('Logged in ' + userinfo.username);
    });
    bot.on('sentry', updateSentry);
    bot.on('error', function(e) {
        console.log(userinfo);
        console.log(e);
    });
    function startIdle(){
        console.log(userinfo.username);
		var myArgs = process.argv.slice(3);
        console.log("Idling game " + myArgs);
        bot.gamesPlayed(myArgs);
        var now = new Date();                
        console.log(now.getHours()+':'+now.getMinutes()+':'+now.getSeconds());
    }
    bot.on('webSessionID', function (sessionID) {
        userinfo.jar = request.jar(),
        userinfo.sessionID = sessionID;
        bot.webLogOn(function(cookies) {
            cookies.forEach(function(cookie) {
                userinfo.jar.setCookie(request.cookie(cookie), 'http://steamcommunity.com');
                userinfo.jar.setCookie(request.cookie(cookie), 'http://store.steampowered.com');
                userinfo.jar.setCookie(request.cookie(cookie), 'https://store.steampowered.com');
            });
            userinfo.jar.setCookie(request.cookie("Steam_Language=english"), 'http://steamcommunity.com');

            startIdle();
            setInterval(function(){startIdle();}, timer);

        });
    });
    bot.logOn({
        accountName: userinfo.username,
        password: userinfo.password,
        authCode: userinfo.authCode,
        shaSentryfile: sentry
    });
}

function createIdler3(userinfo, timer){
    var bot = new steam.SteamClient();
    userinfo.bot = bot;
    bot.on('loggedOn', function() {
        bot.setPersonaState(steam.EPersonaState.Online);
        canTrade = false;
        console.log('Logged in ' + userinfo.username);
    });
    bot.on('sentry', updateSentry);
    bot.on('error', function(e) {
        console.log(userinfo);
        console.log(e);
    });
    function startIdle(){
        var req = request.defaults({jar: userinfo.jar});
        req.get('http://steamcommunity.com/my/badges/', function (err, res, body) {
            if (body) {
		        var bb = body.match(/<a class="btn_green_white_innerfade btn_small_thin" href="steam:\/\/run\/(\d+)">/g);
				var bar = new Array();
				for(i = 0; i < bb.length; i++){
					b = bb[i].match(/<a class="btn_green_white_innerfade btn_small_thin" href="steam:\/\/run\/(\d+)">/);
					if(b[1] != '267420'){
						bar.push(parseInt(b[1]));
					}
				}
		        if (bar) {
		            console.log(userinfo.username);
					console.log(bar);
		            console.log("Idling game " + bar[0]);
					bot.gamesPlayed(bar);
					var now = new Date();                
		    		console.log(now.getHours()+':'+now.getMinutes()+':'+now.getSeconds());
		        }				
            }
        });
    }
    bot.on('webSessionID', function (sessionID) {
        userinfo.jar = request.jar(),
        userinfo.sessionID = sessionID;
        bot.webLogOn(function(cookies) {
            cookies.forEach(function(cookie) {
                userinfo.jar.setCookie(request.cookie(cookie), 'http://steamcommunity.com');
                userinfo.jar.setCookie(request.cookie(cookie), 'http://store.steampowered.com');
                userinfo.jar.setCookie(request.cookie(cookie), 'https://store.steampowered.com');
            });
            userinfo.jar.setCookie(request.cookie("Steam_Language=english"), 'http://steamcommunity.com');

            startIdle();
            setInterval(function(){startIdle();}, timer);

        });
    });
    bot.logOn({
        accountName: userinfo.username,
        password: userinfo.password,
        authCode: userinfo.authCode,
        shaSentryfile: sentry
    });
}

switch(process.argv[2]){
	case '1' : createIdler({username: '', password: '', authCode: ''}, (10*60*1000)); break;
	case '2' : createIdler2({username: '', password: '', authCode: '', game: process.argv[3]}, (10*60*1000)); break;
	case '3' : createIdler3({username: '', password: '', authCode: ''}, (10*60*1000)); break;
}